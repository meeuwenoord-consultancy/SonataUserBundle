<?php

/*
 * This file is part of the Sonata project.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Sonata\UserBundle\GoogleAuthenticator;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class RequestListener
{
    protected $helper;

    protected $security;

    protected $templating;
    protected $container;

    /**
     * @param Helper                                                     $helper
     * @param \Symfony\Component\Security\Core\securityInterface  $security
     * @param \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface $templating
     */
    public function __construct(Helper $helper, TokenStorage $security,
    EngineInterface $templating, $container)
    {
        $this->helper = $helper;
        $this->security = $security;
        $this->templating = $templating;
        $this->container = $container;
    }

    /**
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     *
     * @return
     */
    public function onCoreRequest(GetResponseEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        $token = $this->security->getToken();

        if (!$token) {
            return;
        }

        if (!$token instanceof UsernamePasswordToken) {
            return;
        }

        $key     = $this->helper->getSessionKey($this->security->getToken());
        $request = $event->getRequest();
        $session = $event->getRequest()->getSession();
        $user    = $this->security->getToken()->getUser();

        if (!$session->has($key)) {
            return;
        }

        if ($session->get($key) === true) {
            return;
        }

        $state = 'init';
        if ($request->getMethod() == 'POST') {
            if ($this->helper->checkCode($user, $request->get('_code')) == true) {
                $session->set($key, true);

                return;
            }

            $state = 'error';
        }

        $event->setResponse($this->templating->renderResponse('SonataUserBundle:Admin:Security/two_step_form.html.twig', array(
            'base_template' => 'CMSBackendUIBundle::login_layout.html.twig',
            'admin_pool'    => $this->container->get('sonata.admin.pool'),
            'state'         => $state,
         )));
    }
}
